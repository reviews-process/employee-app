import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEmployeeComponent } from './modules/add-employee/add-employee.component';
import { ListEmployeesComponent } from './modules/list-employees/list-employees.component';


const routes: Routes = [
  {path: 'list-employees', component: ListEmployeesComponent},
  {path: '', redirectTo: '/list-employees', pathMatch: 'full'},
  {path: 'add-employee', component: AddEmployeeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Pipe, PipeTransform } from '@angular/core';
import { areas } from '../services/data/areas';
import { Area } from '../model/area';
import { Role } from '../model/role'; 

@Pipe({
  name: 'role'
})
export class RolePipe implements PipeTransform {

  transform(value: string, areaCode:string): string {
    let roleName:string = '';
    let areaInfo:Area = areas.find(area => area.code === areaCode);
    if( areaInfo !== undefined ) {
      let role:Role = areaInfo.roles.find( role => role.code = value );
      roleName = ( !role ) ? areaCode : role.name;
    }
    return roleName;
  }

}

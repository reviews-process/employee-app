import { Pipe, PipeTransform } from '@angular/core';
import { areas } from '../services/data/areas';
import { Area } from '../model/area';

@Pipe({
  name: 'area'
})
export class AreaPipe implements PipeTransform {

  transform(value: string): string {
    let areaInfo:Area = areas.find(area => area.code === value);
    return (areaInfo === undefined) ? value : areaInfo.name;
  }

}

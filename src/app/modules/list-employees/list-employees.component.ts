import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../model/employee';

@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {

  bossId:string;
  bossName:string;

  employees:Employee[];

  msgResponse:{success:boolean, msg:string} = {success:true, msg: null};

  constructor(
    private readonly route:ActivatedRoute,
    private readonly employeeService:EmployeeService
  ) { }

  ngOnInit(): void {
    this.getAll();
  }

  public getAll():void {
    this.employeeService.getEmployees().subscribe(
      (response:any)=> {
        this.employees = response.employees;    
        this.bossId = null;
        this.bossName = null;
        this.publishMessage(true);
      },err => {
        this.publishMessage(false, `${err.error.issue.code} - ${err.error.issue.message}`)
      }
    )
  }

  public getByBoss(bossInfo:any):void {    
    this.employeeService.getEmployees(bossInfo.bossId).subscribe(
      (response:any)=> {
        this.employees = response.employees;    
        this.bossId = bossInfo.bossId;
        this.bossName = bossInfo.bossName;  
        this.publishMessage(true);
      },err => {
        this.publishMessage(false, `${err.error.issue.code} - ${err.error.issue.message}`)
      }
    )
  }

  private publishMessage(success?:boolean, msg?:string) {
    this.msgResponse.success = success;
    this.msgResponse.msg = msg;
    
  }

}

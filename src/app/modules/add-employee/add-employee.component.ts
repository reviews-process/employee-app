import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

import { areas } from '../../services/data/areas';
import { Employee } from '../../model/employee';
import { Area } from '../../model/area';
import { EmployeeService } from '../../services/employee.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  bossId:string;
  areas:Area[];

  newEmployee:Employee;

  msgResponse:{success:boolean, msg:string} = {success:true, msg: null};

  constructor(
    private readonly route:ActivatedRoute,
    private readonly employeeService:EmployeeService
    ) { }

  ngOnInit(): void {
    const paramMap = this.route.snapshot.queryParamMap;
    if( paramMap.has('bossId') ) {
      this.bossId = this.route.snapshot.queryParamMap.get('bossId');
    }
    this.initializeEmployee();
    this.areas = areas;
  }

  public addEmployee(f:NgForm):void {
    this.employeeService.addEmployee(this.newEmployee).subscribe(
      (response:any)=>{
        this.msgResponse.success = true;
        this.msgResponse.msg = 'Employee was created Successfully!';
        this.initializeEmployee();   
        f.reset();            
      }, err => {        
        this.msgResponse.success = false;
        this.msgResponse.msg = `${err.error.issue.code} - ${err.error.issue.message}`;
      }
    )
  }

  private initializeEmployee():void {
    this.newEmployee = new Employee();
    this.newEmployee.bossId.idNumber = this.bossId;
  }

}

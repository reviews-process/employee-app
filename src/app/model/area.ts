import { Role } from "./role";

export interface Area {
    name:string;
    code:string;
    roles:Role[];
}
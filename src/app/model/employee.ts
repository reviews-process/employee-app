import { Area } from "./area";

export class Employee {
    idNumber:string;
    fullName:string;
    area: Area;
    role:string;
    bossId = {} as Employee;
    employees:any[];
}
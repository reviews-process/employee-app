import {Role} from '../../model/role';
import {Area} from '../../model/area';

const hrRoles:Role[] = [
    {code: "HRA", name:"RH Analyst"},
    {code: "HRO", name:"RH Officer"}
];

const itRoles:Role[] = [
    {code: "ITF", name:"Frontend Developer"},
    {code: "ITB", name:"Backend Developer"},
    {code: "ITL", name:"Technical Leader"},
    {code: "ITA", name:"Software Architect"}
];

const mgRoles:Role[] = [
    {code: "MGA", name:"General Assistant"},
    {code: "MGI", name:"CIO"},
    {code: "MGE", name:"CEO"}
];

export const areas:Area[] = [
    {code: "HR", name: "Human Resources", roles: hrRoles},
    {code: "IT", name: "IT", roles: itRoles},
    {code: "MG", name: "Management", roles: mgRoles}
];
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Employee } from '../model/employee'; 

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private readonly endPoint:string = environment.backendUrl;

  constructor(
    private readonly httpClient: HttpClient
  ) { }

  public addEmployee(employee:Employee):Observable<any> {    

    const bossId = (employee.bossId.idNumber && employee.bossId.idNumber.trim() !== '') ? 
    {
      idNumber: employee.bossId.idNumber
    } : null;

    const request = {      
      employee: {
        idNumber: employee.idNumber,
        fullName: employee.fullName,
        area: employee.area.code,
        role: employee.role,
        bossId
      }
    }

    return this.httpClient.put<any>(`${this.endPoint}employees`, request).pipe(
      map((response:any) => response),
      catchError(e => {
        return throwError(e);
      })
    );
  }

  public getEmployees(bossId?:string):Observable<any> {
    const url:string = (bossId === undefined) ? `${this.endPoint}employees` : `${this.endPoint}employees/boss/${bossId}`;    
    return this.httpClient.get<any>(url).pipe(
      map((response: any) => response),
      catchError( e => {
        return throwError(e);
      })
    );
  }

}
